Make sure to export your Tiled maps in the "Flare map files (*.txt)" format.

any layer with "above" in the name will be rendered above players

any layer with "collision" in the name won't be displayed and be used
to know whether a player can go there (if =0, nothing is there),
or if they cannot walk on that tile (if >0).
